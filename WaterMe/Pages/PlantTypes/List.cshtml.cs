using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using WaterMe.Data;
using WaterMe.Core;


namespace MyApp.Namespace.PlantTypes
{
    public class ListModel : PageModel
    {

        private readonly IConfiguration config;
        private readonly IPlantTypeData plantTypeData;
        public string Message { get; set; }
        public IEnumerable<PlantType> PlantTypes {get; set; }

        public ListModel(IConfiguration config, IPlantTypeData plantTypeData)
        {
            this.plantTypeData = plantTypeData;
            this.config = config;
        }

        //Retrieve the plant data to show in the page
        public void OnGet()
        {
            Message = config["Message"];
            PlantTypes = plantTypeData.GetAll();
        }
    }
}
