using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WaterMe.Core;
using WaterMe.Data;

namespace MyApp.Namespace.PlantTypes
{
    public class DetailModel : PageModel
    {
        private readonly IPlantTypeData plantTypeData;
        public PlantType PlantType {get; set; }

        [TempData]
        public string Message {get; set;}

        public DetailModel(IPlantTypeData plantTypeData){
             this.plantTypeData = plantTypeData;
        }

        //Retrieves the data from the given plantTypeID. OnGet is an action, and a result will be returned.
        // In case there is no plant type with the given idea, show the NotFound page instead
        public IActionResult OnGet(int PlantTypeID)
        {
            PlantType = plantTypeData.GetByID(PlantTypeID);
            if(PlantType == null)
            {
                return RedirectToPage("./NotFound");
            }
            return Page();
        }
    }
}
