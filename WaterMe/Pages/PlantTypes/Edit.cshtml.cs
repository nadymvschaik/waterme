using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using WaterMe.Core;
using WaterMe.Data;

namespace MyApp.Namespace.PlantTypes
{
    public class EditModel : PageModel
    {
        private readonly IPlantTypeData plantTypeData;
        private readonly IHtmlHelper htmlHelper;

        [BindProperty]
        public PlantType PlantType { get; set; }
        public IEnumerable<SelectListItem> SunIntensities {get; set;}
        public EditModel(IPlantTypeData plantTypeData, IHtmlHelper htmlHelper)
        {
            this.plantTypeData = plantTypeData;
            this.htmlHelper = htmlHelper;
        }

        //retrieve plant type data in case a plant with that ID exists. Otherwise, redirect to the NotFound page
        public IActionResult OnGet(int PlantTypeID)
        {
            SunIntensities = htmlHelper.GetEnumSelectList<PlantType.SunIntensity>();
            PlantType = plantTypeData.GetByID(PlantTypeID);
            if(PlantType == null)
            {
                return RedirectToPage("./NotFound");
            }
            return Page();
        }

        //When the save button has been clicked, do the following
        public IActionResult OnPost()
        {
            if(ModelState.IsValid)
            {
                plantTypeData.Update(PlantType);
                plantTypeData.Commit();
                return RedirectToPage("./Detail", new { PlantTypeID = PlantType.PlantTypeID });
            }
            return Page();
        }

    }
}

