using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using WaterMe.Core;
using WaterMe.Data;

namespace MyApp.Namespace.Households
{
    public class CreateModel : PageModel
    {
        private readonly IHouseholdData householdData;
        private readonly IHtmlHelper htmlHelper;


        [BindProperty]
        public Household Household { get; set; }
        public IEnumerable<Household> Households {get; set; }

        public CreateModel(IHouseholdData householdData, IHtmlHelper htmlHelper)
        {
            this.householdData = householdData;
            this.htmlHelper = htmlHelper;
        }

        //retrieve plant data in case a plant with that ID exists. Otherwise, redirect to the NotFound page
        public IActionResult OnGet()
        {   
                 
            Household = new Household();
            
            return Page();
        }

        //When the save button has been clicked, do the following
        public IActionResult OnPost()
        {
//          int ID = Int32.Parse(Request.Form["ID"]);
            if(ModelState.IsValid)
            {
                householdData.Add(Household);
                householdData.Commit();
                TempData["Message"] = "Household saved!";
                return RedirectToPage("./Detail", new { ID = Household.ID });
            } 
            else
            {
                return Page();
            }
            
        }

    }
}
