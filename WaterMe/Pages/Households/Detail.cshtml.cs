using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WaterMe.Core;
using WaterMe.Data;

namespace MyApp.Namespace.Households
{
    public class DetailModel : PageModel
    {
        private readonly IHouseholdData householdData;
        
        public Household Household {get; set; }

        [TempData]
        public string Message {get; set;}

        public DetailModel(IHouseholdData householdData){
             this.householdData = householdData;
             
        }

        //Retrieves the data from the given plantID. OnGet is an action, and a result will be returned.
        // In case there is no plant with the given idea, show the NotFound page instead
        public IActionResult OnGet(int ID)
        {
            Household = householdData.GetByID(ID);
            if(Household == null)
            {
                return RedirectToPage("./NotFound");
            }
            return Page();
        }
    }
}
