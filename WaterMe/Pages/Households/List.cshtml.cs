using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using WaterMe.Data;
using WaterMe.Core;

namespace MyApp.Namespace.Households
{
    public class ListModel : PageModel
    {

        private readonly IConfiguration config;
        private readonly IHouseholdData householdData;
        public string Message { get; set; }
        public IEnumerable<Household> Households {get; set; }

        public ListModel(IConfiguration config, IHouseholdData householdData)
        {
            this.householdData = householdData;
            this.config = config;
        }

        //Retrieve the plant data to show in the page
        public void OnGet()
        {
            Message = config["Message"];
            Households = householdData.GetAll();
        }
    }
}
