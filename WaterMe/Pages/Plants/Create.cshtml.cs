using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using WaterMe.Core;
using WaterMe.Data;

namespace MyApp.Namespace.Plants
{
    public class CreateModel : PageModel
    {
        private readonly IPlantData plantData;
        private readonly IPlantTypeData plantTypeData;
        private readonly IHtmlHelper htmlHelper;


        [BindProperty]
        public Plant Plant { get; set; }
        public IEnumerable<Household> Households {get; set; }
        public IEnumerable<PlantType> PlantTypes {get; set; }

        public CreateModel(IPlantData plantData, IHtmlHelper htmlHelper, IPlantTypeData plantTypeData)
        {
            this.plantData = plantData;
            this.htmlHelper = htmlHelper;
            this.plantTypeData = plantTypeData;
        }

        //retrieve plant data in case a plant with that ID exists. Otherwise, redirect to the NotFound page
        public IActionResult OnGet()
        {   
            PlantTypes = plantTypeData.GetAll();
            
            Plant = new Plant();
            
            return Page();
        }

        //When the save button has been clicked, do the following
        public IActionResult OnPost()
        {
            int plantTypeID = Int32.Parse(Request.Form["plantTypeID"]);
            PlantTypes = plantTypeData.GetAll();
            if(ModelState.IsValid)
            {
                PlantType plantType = plantTypeData.GetByID(plantTypeID);
                Plant.PlantType = plantType;

                plantData.Add(Plant);
                plantData.Commit();
                TempData["Message"] = "Plant saved!";
                return RedirectToPage("./Detail", new { PlantID = Plant.PlantID });
            } 
            else
            {
                return Page();
            }
            
        }

    }
}
