using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using WaterMe.Data;
using WaterMe.Core;

namespace MyApp.Namespace.Plants
{
    public class ListModel : PageModel
    {

        private readonly IConfiguration config;
        private readonly IPlantData plantData;
        public string Message { get; set; }
        public IEnumerable<Plant> Plants {get; set; }

        public ListModel(IConfiguration config, IPlantData plantData)
        {
            this.plantData = plantData;
            this.config = config;
        }

        //Retrieve the plant data to show in the page
        public void OnGet()
        {
            Message = config["Message"];
            Plants = plantData.GetAll();
        }
    }
}
