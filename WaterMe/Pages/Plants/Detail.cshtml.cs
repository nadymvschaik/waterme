using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WaterMe.Core;
using WaterMe.Data;

namespace MyApp.Namespace.Plants
{
    public class DetailModel : PageModel
    {
        private readonly IPlantData plantData;
        
        public Plant Plant {get; set; }

        [TempData]
        public string Message {get; set;}

        public DetailModel(IPlantData plantData){
             this.plantData = plantData;
             
        }

        //Retrieves the data from the given plantID. OnGet is an action, and a result will be returned.
        // In case there is no plant with the given idea, show the NotFound page instead
        public IActionResult OnGet(int PlantID)
        {
            Plant = plantData.GetByID(PlantID);
            if(Plant == null)
            {
                return RedirectToPage("./NotFound");
            }
            return Page();
        }
    }
}
