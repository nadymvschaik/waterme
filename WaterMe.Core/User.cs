using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WaterMe.Core
{

    public class User
    {   
        public User() 
        {
            this.Households = new HashSet<Household>();
        }

        [Required]
        public int ID{ get; set; }
        
        public string Name { get; set; }

        public virtual ICollection<Household> Households { get; set; }
    }
}
   