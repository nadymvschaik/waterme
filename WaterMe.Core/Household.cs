using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WaterMe.Core
{

    public class Household{

        public int ID {get; set;}

        public String Name {get; set;}

        
        public Household()
        {
            this.Users = new HashSet<User>();
        }

        public virtual ICollection<User> Users { get; set; }
    }
}