using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WaterMe.Core
{
    public class Plant
    {   

        public int PlantID {get; set; }

        [Required]
        [StringLength(30)]
        public string PlantName {get; set; }

        public DateTime DateLastWatered {get; set; }

        //Foreign key for PlantType
        public int PlantTypeID{get; set;}
        public PlantType PlantType {get; set; }

        //Foreign key for Household
        public int HouseholdID {get; set;}
        public Household Household {get;set;}
    }
}