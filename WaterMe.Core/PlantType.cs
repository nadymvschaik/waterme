using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace WaterMe.Core
{
    public class PlantType
    {
        public int PlantTypeID {get; set; }

        public ICollection<Plant> Plants {get; set;}

        [Required]
        [StringLength(30)]
        public string Name {get; set; }
        
        public SunIntensity Intensity {get; set;}

        [Required]
        public int WateringInterval {get; set; }

        [StringLength(50)]
        public string ScientificName {get; set; }
        
        public enum SunIntensity {
            Sunny,
            HalfShadow,
            Shadow
        }
    }
}