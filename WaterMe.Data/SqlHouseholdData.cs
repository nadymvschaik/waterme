using WaterMe.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace WaterMe.Data
{
    public class SqlHouseholdData : IHouseholdData
    {
        private readonly WaterMeDbContext db;

        private IPlantData plantData;

        public SqlHouseholdData(WaterMeDbContext db, IPlantData plantData)
        {
            this.db = db;
            this.plantData = plantData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newHousehold"></param>
        /// <returns>newHousehold</returns>
        public Household Add(Household newHousehold)
        {
            db.Add(newHousehold);
            return newHousehold;
        }

        /// <summary>
        /// Commits saved changes to the database
        /// </summary>
        /// <returns>The number of state entries written to the database.</returns>
        public int Commit()
        {
            return db.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns>household</returns>
        public Household Delete(int ID)
        {
            var household = GetByID(ID);
            if(household != null){
                db.Households.Remove(household);
            }
            return household;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>All Households as Enumerable</returns>
        public IEnumerable<Household> GetAll()
        {
            var query = from h in db.Households
                        select h;
            return query;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns>Household with given ID</returns>
        public Household GetByID(int ID)
        {
            Household household = db.Households.Find(ID);
            return household;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="updatedHousehold">Household entity with updated attributes</param>
        /// <returns> updatedHousehold object </returns>
        public Household Update(Household updatedHousehold)
        {
            var entity = db.Households.Attach(updatedHousehold);
            entity.State = EntityState.Modified;
            return updatedHousehold;
        }
    }
}
