using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using WaterMe.Core;
using WaterMe;

namespace WaterMe.Data
{
    public class WaterMeDbContext : DbContext
    {
        public WaterMeDbContext(DbContextOptions<WaterMeDbContext> options) : base(options)
        {
            
        }
        
        public DbSet<Plant> Plants {get; set;}
        public DbSet<PlantType> PlantTypes {get; set;}

        public DbSet<Household> Households {get; set;}

        public DbSet<User> Users {get; set;}
    }
}