using WaterMe.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace WaterMe.Data
{
    public class SqlPlantTypeData : IPlantTypeData
    {
        private readonly WaterMeDbContext db;

        public SqlPlantTypeData(WaterMeDbContext db)
        {
            this.db = db;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newPlantType"></param>
        /// <returns>newPlantType</returns>
        public PlantType Add(PlantType newPlantType)
        {
            db.Add(newPlantType);
            return newPlantType;
        }

        /// <summary>
        /// Commits saved changes to the databas
        /// </summary>
        /// <returns>The number of state entries written to the database</returns>
        public int Commit()
        {
            return db.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PlantTypeID"></param>
        /// <returns>plantType</returns>
        public PlantType Delete(int PlantTypeID)
        {
            var plantType = GetByID(PlantTypeID);
            if(plantType != null){
                db.PlantTypes.Remove(plantType);
            }
            return plantType;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns>All PlantTypes as Enumerable</returns>
        public IEnumerable<PlantType> GetAll()
        {
            var query = from p in db.PlantTypes
                        select p;

            return query;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PlantTypeID"></param>
        /// <returns>PlantType with given PlantTypeID</returns>
        public PlantType GetByID(int PlantTypeID)
        {
            return db.PlantTypes.Find(PlantTypeID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="updatedPlantType">Plant entity with updated attributes</param>
        /// <returns>updatedPlant object</returns>
        public PlantType Update(PlantType updatedPlantType)
        {
            var entity = db.PlantTypes.Attach(updatedPlantType);
            entity.State = EntityState.Modified;
            return updatedPlantType;
        }
    }
}