﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WaterMe.Data.Migrations
{
    public partial class adding_user_and_household : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "HouseholdID",
                table: "Plants",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Household",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Household", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    HouseholdID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.ID);
                    table.ForeignKey(
                        name: "FK_User_Household_HouseholdID",
                        column: x => x.HouseholdID,
                        principalTable: "Household",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Plants_HouseholdID",
                table: "Plants",
                column: "HouseholdID");

            migrationBuilder.CreateIndex(
                name: "IX_User_HouseholdID",
                table: "User",
                column: "HouseholdID");

            migrationBuilder.AddForeignKey(
                name: "FK_Plants_Household_HouseholdID",
                table: "Plants",
                column: "HouseholdID",
                principalTable: "Household",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Plants_Household_HouseholdID",
                table: "Plants");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "Household");

            migrationBuilder.DropIndex(
                name: "IX_Plants_HouseholdID",
                table: "Plants");

            migrationBuilder.DropColumn(
                name: "HouseholdID",
                table: "Plants");
        }
    }
}
