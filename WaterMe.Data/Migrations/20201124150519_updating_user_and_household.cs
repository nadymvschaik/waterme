﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WaterMe.Data.Migrations
{
    public partial class updating_user_and_household : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_User_Household_HouseholdID",
                table: "User");

            migrationBuilder.DropIndex(
                name: "IX_User_HouseholdID",
                table: "User");

            migrationBuilder.DropColumn(
                name: "HouseholdID",
                table: "User");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "User",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "HouseholdUser",
                columns: table => new
                {
                    HouseholdsID = table.Column<int>(type: "int", nullable: false),
                    UsersID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HouseholdUser", x => new { x.HouseholdsID, x.UsersID });
                    table.ForeignKey(
                        name: "FK_HouseholdUser_Household_HouseholdsID",
                        column: x => x.HouseholdsID,
                        principalTable: "Household",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HouseholdUser_User_UsersID",
                        column: x => x.UsersID,
                        principalTable: "User",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_HouseholdUser_UsersID",
                table: "HouseholdUser",
                column: "UsersID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HouseholdUser");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "User");

            migrationBuilder.AddColumn<int>(
                name: "HouseholdID",
                table: "User",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_User_HouseholdID",
                table: "User",
                column: "HouseholdID");

            migrationBuilder.AddForeignKey(
                name: "FK_User_Household_HouseholdID",
                table: "User",
                column: "HouseholdID",
                principalTable: "Household",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
