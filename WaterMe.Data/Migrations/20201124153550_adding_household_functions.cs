﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WaterMe.Data.Migrations
{
    public partial class adding_household_functions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HouseholdUser_Household_HouseholdsID",
                table: "HouseholdUser");

            migrationBuilder.DropForeignKey(
                name: "FK_HouseholdUser_User_UsersID",
                table: "HouseholdUser");

            migrationBuilder.DropForeignKey(
                name: "FK_Plants_Household_HouseholdID",
                table: "Plants");

            migrationBuilder.DropPrimaryKey(
                name: "PK_User",
                table: "User");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Household",
                table: "Household");

            migrationBuilder.RenameTable(
                name: "User",
                newName: "Users");

            migrationBuilder.RenameTable(
                name: "Household",
                newName: "Households");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Users",
                table: "Users",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Households",
                table: "Households",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_HouseholdUser_Households_HouseholdsID",
                table: "HouseholdUser",
                column: "HouseholdsID",
                principalTable: "Households",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_HouseholdUser_Users_UsersID",
                table: "HouseholdUser",
                column: "UsersID",
                principalTable: "Users",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Plants_Households_HouseholdID",
                table: "Plants",
                column: "HouseholdID",
                principalTable: "Households",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HouseholdUser_Households_HouseholdsID",
                table: "HouseholdUser");

            migrationBuilder.DropForeignKey(
                name: "FK_HouseholdUser_Users_UsersID",
                table: "HouseholdUser");

            migrationBuilder.DropForeignKey(
                name: "FK_Plants_Households_HouseholdID",
                table: "Plants");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Users",
                table: "Users");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Households",
                table: "Households");

            migrationBuilder.RenameTable(
                name: "Users",
                newName: "User");

            migrationBuilder.RenameTable(
                name: "Households",
                newName: "Household");

            migrationBuilder.AddPrimaryKey(
                name: "PK_User",
                table: "User",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Household",
                table: "Household",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_HouseholdUser_Household_HouseholdsID",
                table: "HouseholdUser",
                column: "HouseholdsID",
                principalTable: "Household",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_HouseholdUser_User_UsersID",
                table: "HouseholdUser",
                column: "UsersID",
                principalTable: "User",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Plants_Household_HouseholdID",
                table: "Plants",
                column: "HouseholdID",
                principalTable: "Household",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
