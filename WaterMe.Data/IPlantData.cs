using WaterMe.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace WaterMe.Data
{
    public interface IPlantData
    {
        IEnumerable<Plant> GetAll();
        Plant GetByID(int PlantID);
        Plant Update(Plant updatedPlant);
        int Commit();
        Plant Add(Plant newPlant);
        Plant Delete(int PlantID);
    }
}