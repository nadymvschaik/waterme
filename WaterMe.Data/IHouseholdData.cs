using WaterMe.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace WaterMe.Data
{
    public interface IHouseholdData
    {
        IEnumerable<Household> GetAll();
        Household GetByID(int ID);
        Household Update(Household updatedHousehold);
        int Commit();
        Household Add(Household newHousehold);
        Household Delete(int ID);
    }
}