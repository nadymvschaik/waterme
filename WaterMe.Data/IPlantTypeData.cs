using WaterMe.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace WaterMe.Data
{
    public interface IPlantTypeData
    {
        IEnumerable<PlantType> GetAll();
        PlantType GetByID(int PlantTypeID);
        PlantType Update(PlantType updatedPlantType);
        int Commit();
        PlantType Add(PlantType newPlantType);
        PlantType Delete(int PlantTypeID);
    }
}