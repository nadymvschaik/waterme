using WaterMe.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace WaterMe.Data
{
    public class SqlPlantData : IPlantData
    {
        private readonly WaterMeDbContext db;

        private IPlantTypeData plantTypeData;

        public SqlPlantData(WaterMeDbContext db, IPlantTypeData plantTypeData)
        {
            this.db = db;
            this.plantTypeData = plantTypeData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newPlant"></param>
        /// <returns>newPlant</returns>
        public Plant Add(Plant newPlant)
        {
            db.Add(newPlant);
            return newPlant;
        }

        /// <summary>
        /// Commits saved changes to the database
        /// </summary>
        /// <returns>The number of state entries written to the database.</returns>
        public int Commit()
        {
            return db.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PlantID"></param>
        /// <returns>plant</returns>
        public Plant Delete(int PlantID)
        {
            var plant = GetByID(PlantID);
            if(plant != null){
                db.Plants.Remove(plant);
            }
            return plant;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>All Plants as Enumerable</returns>
        public IEnumerable<Plant> GetAll()
        {
            var query = from p in db.Plants
                        select p;

            // int currentUserId = currentUser.id
            // SELECT p FROM plants AS p INNER JOIN households 
            // INNER JOIN householduser AS hu WHERE hu.userId = currentUserId

            return query;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PlantID"></param>
        /// <returns>Plant with given PlantID</returns>
        public Plant GetByID(int PlantID)
        {
            Plant plant = db.Plants.Find(PlantID);
            if (plant != null) {
                plant.PlantType = this.plantTypeData.GetByID(plant.PlantTypeID);
            }
            return plant;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="updatedPlant">Plant entity with updated attributes</param>
        /// <returns> updatedPlant object </returns>
        public Plant Update(Plant updatedPlant)
        {
            var entity = db.Plants.Attach(updatedPlant);
            entity.State = EntityState.Modified;
            return updatedPlant;
        }
    }
}
