using WaterMe.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace WaterMe.Data
{
    public class InMemoryPlantData: IPlantData
    {
        List<Plant> plants;
        List<PlantType> plantTypes;
        public InMemoryPlantData()
        {
            plantTypes = new List<PlantType>()
            {
                new PlantType{
                    PlantTypeID = 1,
                    Name = "KamerPlant",
                    Intensity = PlantType.SunIntensity.HalfShadow,
                    WateringInterval = 2,
                    ScientificName = "Kamerus Plantus"
                }
            };

            plants = new List<Plant>()
            {
                new Plant{PlantID = 1, PlantName = "Bonsai", DateLastWatered = DateTime.Now, PlantType = plantTypes[0]},
                new Plant{PlantID = 2, PlantName = "Bonsai Woonkamer", DateLastWatered = DateTime.Now, PlantType = plantTypes[0]},
                new Plant{PlantID = 3, PlantName = "Cactus", DateLastWatered = DateTime.Now, PlantType = plantTypes[0]}
            };
        }

        //Retrieve the plant information from the given ID, or give a default (null) value
        public Plant GetByID(int PlantID)
        {
            return plants.SingleOrDefault(r => r.PlantID == PlantID);
        }

        public Plant Add(Plant newPlant)
        {
            plants.Add(newPlant);
            newPlant.PlantID = plants.Max(r => r.PlantID)+1;
            return newPlant;
        }

        //Updates a plant's information after it has been edited
        public Plant Update(Plant updatedPlant)
        {
            var plant = plants.SingleOrDefault(r => r.PlantID == updatedPlant.PlantID);
            if(plant != null)
            {
                plant.PlantName = updatedPlant.PlantName;
                plant.PlantType = updatedPlant.PlantType;
            }
            return plant;
        }

        
        //Commit the changes 
        public int Commit()
        {
            return 0;
        }
        public IEnumerable<Plant> GetAll()
        {
            return from p in plants
                orderby p.PlantName
                select p;
        }

        public Plant Delete(int PlantID)
        {
            var plant = plants.FirstOrDefault(r => r.PlantID == PlantID);
            if(plant != null){
                plants.Remove(plant);
            }
            return plant;
        }
    }
}