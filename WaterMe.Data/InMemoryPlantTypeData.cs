using WaterMe.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace WaterMe.Data
{
    public class InMemoryPlantTypeData: IPlantTypeData
    {
        List<Plant> plants;
        List<PlantType> plantTypes;
        public InMemoryPlantTypeData()
        {
            plantTypes = new List<PlantType>()
            {
                new PlantType{
                    PlantTypeID = 1,
                    Name = "KamerPlant",
                    Intensity = PlantType.SunIntensity.HalfShadow,
                    WateringInterval = 2,
                    ScientificName = "Kamerus Plantus"
                },
                new PlantType{
                    PlantTypeID = 2,
                    Name = "BuitenPlant",
                    Intensity = PlantType.SunIntensity.Shadow,
                    WateringInterval = 5,
                    ScientificName = "Buitenus Plantus"
                },
                new PlantType{
                    PlantTypeID = 3,
                    Name = "CactusPlant",
                    Intensity = PlantType.SunIntensity.Sunny,
                    WateringInterval = 20,
                    ScientificName = "Cactus Plantus"
                }
            };

            plants = new List<Plant>()
            {
                new Plant{PlantID = 1, PlantName = "Bonsai", DateLastWatered = DateTime.Now, PlantType = plantTypes[0]},
                new Plant{PlantID = 2, PlantName = "Bonsai Woonkamer", DateLastWatered = DateTime.Now, PlantType = plantTypes[0]},
                new Plant{PlantID = 3, PlantName = "Cactus", DateLastWatered = DateTime.Now, PlantType = plantTypes[0]}
            };
        }

        //Retrieve the plant Type information from the given ID, or give a default (null) value
        public PlantType GetByID(int PlantTypeID)
        {
            return plantTypes.SingleOrDefault(r => r.PlantTypeID == PlantTypeID);
        }

        public PlantType Add(PlantType newPlantType)
        {
            plantTypes.Add(newPlantType);
            newPlantType.PlantTypeID = plantTypes.Max(r => r.PlantTypeID)+1;
            return newPlantType;
        }

        //Updates a plant type's information after it has been edited
        public PlantType Update(PlantType updatedPlantType)
        {
            var plantType = plantTypes.SingleOrDefault(r => r.PlantTypeID == updatedPlantType.PlantTypeID);
            if(plantType != null)
            {
                plantType.Name = updatedPlantType.Name;
                plantType.WateringInterval = updatedPlantType.WateringInterval;
                plantType.ScientificName = updatedPlantType.ScientificName;
                plantType.Intensity = updatedPlantType.Intensity;
            }
            return plantType;
        }
        
        //Commit the changes 
        public int Commit()
        {
            return 0;
        }
        public IEnumerable<PlantType> GetAll()
        {
            return from p in plantTypes
                orderby p.Name
                select p;
        }

        public PlantType Delete(int PlantTypeID)
        {
            var plantType = plantTypes.FirstOrDefault(r => r.PlantTypeID == PlantTypeID);
            if(plantType != null){
                plantTypes.Remove(plantType);
            }
            return plantType;
        }
    }
}
